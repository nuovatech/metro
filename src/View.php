<?php

namespace Template\Metro\Src;

use \Neon\Core\Neon;
use \Neon\Core\View as NeonView;

use Directory;

/**
 * Classe de gestão dos recursos das views da aplicação.
 * @author Eduardo Marinho
 * @since  18/06/2021
 * @version 0.0.2.20220203
 */
abstract class View extends NeonView
{

    /** Método de inclusão da TAG LINK em VIEWS.
     * @param   string    $url Caminho do arquivo css
     * @param   string    $place local de carregamento do recurso.
     *                      - vazio: carregamento da pasta do projeto;
     *                      - framework: carregamento da biblioteca do framework;
     *                      - template: carregamento do template
     * @author  Eduardo Marinho
     * @version 0.0.20210609
     */
    public static function css(string $url, string $place = null)
    {
        if (empty($place)) {
            $url = self::getUri() . "public/assets/css/$url.css";
        }

        if ($place == "framework") {
        }

        if ($place == "template") {
            $url = self::getUri() . "public/templates/metro/assets/css/$url.css";
        }
        echo "<link href='$url' rel='stylesheet' type='text/css'>";
    }

    /**
     * Método de importação de recustos adicionais
     * @param string $path Caminho do arquivo que será carregado
     * @param string $extensão do arquivo
     */
    public static function extra(string $path, string $extension)
    {
        $url = self::getUri() . "public/templates/metro/assets/etc/$path.";

        switch ($extension) {
            case "css": {
                    echo '<link href="' . $url . 'css" rel="stylesheet" type="text/css">';
                    break;
                }
            case "js": {
                    echo '<script src="' . $url . 'js">' . "</script>";
                    break;
                }
            default: {
                    echo '<div class="warning --danger">View::extra(). Extensão do arquivo é inválida!</div>';
                    break;
                }
        }
    }

    /**
     * Método responsável por renderizar ícones do pacote do template.
     * @param   string $icon nome da imagem do ícone
     * @param   string $size tamanho do ícone
     */
    public static function icon(string $icon, string $size = "18px", bool $print = false)
    {
        // Caminho absoluto da imagem do template
        $url = self::getUri() . "public/templates/metro/assets/images/icons/" . $icon . '.svg';
        if ($print == true) {
            return "<img src='$url' width='$size' >";
        } else {
            echo "<img src='$url' width='$size' >";
        }
    }

    public static function svg(string $icon, int $size = 18, string $color = "#fff", bool $return = false)
    {
        $icon = "public/templates/metro/assets/images/icons/" . strtolower($icon)  . '.svg';
        $svg = file_exists($icon) ? file_get_contents($icon) : '*';
        $img = " <div class='metro-icon' style='width: {$size}px; margin: auto; fill: {$color}; height: {$size}px'>$svg</div>";
        if ($return == true) {
            return $img;
        } else {
            echo $img;
        }
    }

    /**
     * Método de inclusão da TAG SCRIPT em VIEWS.
     * @param string $url Caminho do arquivo js
     * @param string $template Indica a fonte do carremgamento (null, da própría aplicação, "framework" do framework, "template" carregamento do template)
     * @param bool  $defer Habilita o carregamento posterior do script
     * @see https://www.w3schools.com/tags/att_script_defer.asp
     * @author Eduardo Marinho
     * @version 0.0.20210609
     */
    public static function script($url, $template = false, $defer = null)
    {

        if (empty($template)) {
            $url = self::getUri() . "public/assets/script/$url.js";
        }

        if ($template == true) {
            $url = self::getUri() . "public/templates/metro/assets/script/$url.js";
        }
        echo ($defer) ? "<script defer src='$url'>" . "</script>" :  "<script src='$url'>" . "</script>";
    }

    /**
     * Método utilizado para inclusão de módulo de script do template
     * @param   string  $module módulo que será carregado
     * @author  Eduardo Marinho
     */
    public static function module(string $module)
    {
        $url = self::getUri() . "public/templates/metro/assets/script/modules/$module.js";
        echo "<script src='$url'></script>";
    }

    static private function getUri()
    {
        // Obtem o endereço da raíz do index
        $urix = explode('/', $_SERVER['PHP_SELF']);

        // Remove o index.php do array
        unset($urix[count($urix) - 1]);

        // Remonta o caminho utilizando a URL editada
        return  $_SERVER['REQUEST_SCHEME'] . "://" .  $_SERVER['SERVER_NAME'] . implode('/', $urix) . '/';
    }

    /**
     * Método responsável por realizar a renderização da (view) no template.
     * @param string $path caminho do arquivo
     * @param string $type formato do arquivo (html, php)
     * @param array $vars variáveis com parâmetros
     */
    public static function template(string $path, array $vars = [], string $type = null)
    {

        // Passa os parâmetros para a variável global
        parent::setGlobal($vars);

        // Limpa a variável de passagem de parâmetro
        unset($vars);

        // Carregamento do layout
        $layout = "public/templates/metro/layout.php";

        // Verifica a extensão do arquivo
        $extension = ($type === null) ? "php" : $type;

        // Monta o caminho do arquivo
        $view = "public/pages/" . $path . '.' . $extension;

        // Atribui a carga do conteúdo à variável global
        $body = file_exists($view) ? file_get_contents($view) : '';

        ob_start();
        eval("?>" . $body  . "<?php");
        $body  = ob_get_clean();
        parent::setBody($body);
        require $layout;
    }

    /**
     * Método responsável por realizar a renderização das visões de páginas HTTP
     * @param int    $status código do status  http
     * @param string $message mensagem de erro da exceção
     */
    public static function http(int $status, string $message = '')
    {
        View::setBody(null);

        switch ($status) {
            case 403: {
                    View::render("http/status", [
                        "msg" => "Acesso Negado, você não tem permissão para acessar esse conteúdo",
                        "site" => "Meyor's Admin: 403",
                        "status" => 403
                    ]);
                    break;
                }
            case 404: {
                    View::render("http/status", [
                        "msg" => "Página ou arquivo não encontrado. Verifique o endereço e tente novamente.",
                        "site" => "Meyor's Admin: 404",
                        "status" => 404
                    ]);
                    break;
                }
            default:
                View::render("http/status", [
                    "msg" => $message,
                    "site" => "Meyor's Admin: $status",
                    "status" => $status
                ]);
                break;
        }
        exit;
    }
}
