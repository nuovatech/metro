/**
 * @description Objeto de validação para campos
 * @author  Eduardo Marinho
 * @see     ed.marinho@outlook.com
 */
const Validation = {

    /**
     * @description Método de validação de CPF
     * @author  DevMedia
     * @see https://www.devmedia.com.br/validar-cpf-com-javascript/23916
     * @param {string} cpf 
     * @returns bool
     */
    cpf: function (cpf) {

        // remove a máscara do cpf caso possua
        cpf = cpf.replace(/[^0-9]/g, '');

        switch (cpf) {
            case
                "00000000000",
                "11111111111",
                "22222222222",
                "33333333333",
                "44444444444",
                "55555555555",
                "66666666666",
                "77777777777",
                "88888888888",
                "99999999999": {
                    return false;
                    break;
                }
            default: {

                var Soma = 0;
                var Resto;

                for (i = 1; i <= 9; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto == 10) || (Resto == 11)) Resto = 0;
                if (Resto != parseInt(cpf.substring(9, 10))) return false;

                Soma = 0;
                for (i = 1; i <= 10; i++) Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto == 10) || (Resto == 11)) Resto = 0;
                if (Resto != parseInt(cpf.substring(10, 11))) return false;
                return true;
                break;
            }
        }
    },

    /**
     * @description Método de validação de e-mail
     * @author  DevMedia
     * @see https://www.devmedia.com.br/validando-e-mail-em-inputs-html-com-javascript/26427
     * @param {string} email 
     * @returns bool
     */
    email: function (email) {

        usuario = email.substring(0, email.indexOf("@"));
        dominio = email.substring(email.indexOf("@") + 1, email.length);

        if ((usuario.length >= 1) &&
            (dominio.length >= 3) &&
            (usuario.search("@") == -1) &&
            (dominio.search("@") == -1) &&
            (usuario.search(" ") == -1) &&
            (dominio.search(" ") == -1) &&
            (dominio.search(".") != -1) &&
            (dominio.indexOf(".") >= 1) &&
            (dominio.lastIndexOf(".") < dominio.length - 1)) {
            return true;
        }
        else {
            return false;
        }
    }
}