$(function () {
	// Gerenciamento das abas
	$(".tab__collector a").click(function () {

		// Gerencia o seletor das abas
		$(".tab__collector a").removeClass("--active");
		$(this).addClass("--active");

		// Abas de conteÃºdo
		$(".tab__container .tab").removeClass("--active").hide();
		let bind = $(this).attr("data-bind");
		$("#tab-" + bind).fadeIn();
	});
});
